<?php
/**
 * fnSilex index
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    require_once __DIR__ . '/../vendor/autoload.php';

    $app = new Silex\Application();

    require_once __DIR__ . '/../resources/config/prod.php';
    require_once __DIR__ . '/../app/Bootstrap.php';
    require_once __DIR__ . '/../app/Routes.php';

    $app['http_cache']->run();
