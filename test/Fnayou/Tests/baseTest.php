<?php

    namespace Fnayou\Tests;

    use Silex\Application;
    use Silex\WebTestCase;

    class baseTest extends WebTestCase
    {
        public function createApplication()
        {
            $app = new Application();

            require __DIR__ . '/../../../resources/config/test.php';
            require __DIR__ . '/../../../app/Bootstrap.php';
            require __DIR__ . '/../../../app/Routes.php';

            return $app;
        }

        public function test404()
        {
            $client = $this->createClient();

            $client->request('GET', '/get-404');
            $this->assertEquals(404, $client->getResponse()->getStatusCode());
        }

        public function testIndex()
        {
            $client  = $this->createClient();
            $crawler = $client->request('GET', '/');

            $this->assertTrue($client->getResponse()->isOk());
            $this->assertCount(1, $crawler->filter('h1:contains("Hello World")'));
            $this->assertCount(1, $crawler->filter('html:contains("Welcome to fnSilex - Simple Silex Boilerplate !")'));
        }

    }
