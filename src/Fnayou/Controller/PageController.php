<?php
/**
 * fnSilex main controller
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
namespace Fnayou\Controller;

    use Silex\Application;
    use Symfony\Component\HttpFoundation\Request;

    class PageController
    {

        public function homepageAction(Request $request, Application $app)
        {

            //here you an do all the magic

            //views path : project_root_path\resources\views
            return $app['twig']->render('Page\homepage.html.twig', array());
        }

    }
