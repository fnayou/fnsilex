<?php
/**
 * The task will be loaded once composer update or install
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    namespace Fnayou\Task;

    class Jobs
    {
        public static function initiate()
        {
            chmod('app/cache', 0777);
            chmod('app/log', 0777);
            chmod('web/css', 0777);
            chmod('web/js', 0777);
            chmod('web/images', 0777);
            chmod('app/console', 0500);
            exec('php app/console assetic:dump');
            exec('php app/console cache:clear');
        }
    }
