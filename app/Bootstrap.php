<?php
/**
 * Bootstrap
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    use Silex\Provider\HttpCacheServiceProvider;
    use Silex\Provider\MonologServiceProvider;
    use Silex\Provider\TwigServiceProvider;
    use SilexAssetic\AsseticServiceProvider;

    // register Twig service
    $app->register(new TwigServiceProvider(), array(
        'twig.options' => array(
            'cache'            => isset($app['twig.options.cache']) ? $app['twig.options.cache'] : false,
            'strict_variables' => true
        ),
        'twig.path'    => array(__DIR__ . '/../resources/views')
    ));

    // register http cache service
    $app->register(new HttpCacheServiceProvider());

    // register monolog service
    $app->register(new MonologServiceProvider(), array(
        'monolog.logfile' => __DIR__ . '/../app/log/fnsilex.log',
        'monolog.name'    => 'fnsilex',
        'monolog.level'   => 300 // = Logger::WARNING
    ));

    // register assetic service
    if (isset($app['assetic.enabled']) && $app['assetic.enabled']) {
        $app->register(new AsseticServiceProvider(), array(
            'assetic.options' => array(
                'debug' => $app['debug'],
                'auto_dump_assets' => $app['debug'],
            )
        ));

        $app['assetic.asset_manager'] = $app->share(
            $app->extend('assetic.asset_manager', function ($am, $app) {

                $am->set('styles', new Assetic\Asset\AssetCache(
                    new Assetic\Asset\GlobAsset($app['assetic.input.path_to_css']),
                    new Assetic\Cache\FilesystemCache($app['assetic.path_to_cache'])
                ));
                $am->get('styles')->setTargetPath($app['assetic.output.path_to_css']);

                $am->set('scripts', new Assetic\Asset\AssetCache(
                    new Assetic\Asset\GlobAsset($app['assetic.input.path_to_js']),
                    new Assetic\Cache\FilesystemCache($app['assetic.path_to_cache'])
                ));
                $am->get('scripts')->setTargetPath($app['assetic.output.path_to_js']);

                return $am;
            })
        );

    }

    return $app;
