<?php
/**
 * Console
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    use Symfony\Component\Console\Application;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Filesystem\Filesystem;
    use Symfony\Component\Finder\Finder;
    use Fnayou\Task\Jobs as Jobs;

    // initiate console
    $console = new Application('fnSilex - Simple Silex Boilerplate', '0.1');

    $app->boot();

    //register fnsilex:initiate task
    $console
    ->register('fnsilex:initiate')
    ->setDescription('initiate fnSilex project by proceeding permissions and assetic and cache')
    ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

        Jobs::initiate();

        $output->writeln(sprintf("%s <info>Project initiated</info>", 'fnSilex : '));
    });

    // register assetic:dump task
    $console
        ->register('assetic:dump')
        ->setDescription('dump all assets to the web directory')
        ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {
            if (!$app['assetic.enabled']) {
                return false;
            }

            $dumper = $app['assetic.dumper'];
            if (isset($app['twig'])) {
                $dumper->addTwigAssets();
            }
            $dumper->dumpAssets();
            
            $output->writeln(sprintf("%s <info>Files Dumped</info>", 'fnSilex : '));
        })
    ;

    // register cache:clear task
    if (isset($app['cache.path'])) {
        $console
            ->register('cache:clear')
            ->setDescription('clear cache')
            ->setCode(function (InputInterface $input, OutputInterface $output) use ($app) {

                $cacheDir = $app['cache.path'];
                $finder = Finder::create()->in($cacheDir)->notName('.gitkeep');

                $filesystem = new Filesystem();
                $filesystem->remove($finder);

                $output->writeln(sprintf("%s <info>Cache Cleared</info>", 'fnSilex : '));
            });
    }

    return $console;
