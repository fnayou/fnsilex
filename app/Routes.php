<?php
/**
 * Routes
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    use Silex\Application\TwigTrait;
    use Silex\Application;
    use Symfony\Component\HttpFoundation\Response;

    // the homepage route
    $app->get('/', 'Fnayou\Controller\PageController::homepageAction');

    // deal with Exception errors
    $app->error(function (\Exception $e, $code) use ($app) {
        switch ($code) {
            case 404:
                $message = $app['twig']->render('Error\error404.html.twig');
                break;
            default:
                $message = 'Oops ! Something went wrong.';
        }

        if ($app['debug']) {
            $message .= ' Error Message: ' . $e->getMessage();
        }

        return new Response($message, $code);
    });

    return $app;
