<?php

/**
 * Configuration for production environment
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    // Localisation Settings
    $app['locale'] = 'en';
    $app['session.default_locale'] = $app['locale'];

    // Cache Settings
    $app['cache.path'] = __DIR__ . '/../../app/cache';

    // HTTP Cache Settings
    $app['http_cache.cache_dir'] = $app['cache.path'] . '/http';

    // Twig Settings
    $app['twig.options.cache'] = $app['cache.path'] . '/twig';

    // Assetic Settings
    $app['assetic.enabled'] = true;
    $app['assetic.path_to_cache'] = $app['cache.path'] . '/assetic' ;
    $app['assetic.path_to_web'] = __DIR__ . '/../../web';
    $app['assetic.input.path_to_assets'] = __DIR__ . '/../assets';

    // you can leave it as bellow or point to each css file
    $app['assetic.input.path_to_css']  = array(
        $app['assetic.input.path_to_assets'] . '/css/*.css',
    );
    $app['assetic.output.path_to_css'] = 'css/fnsilex.css';

    // you can leave it as bellow or point to each js file
    $app['assetic.input.path_to_js'] = array(
        $app['assetic.input.path_to_assets'] . '/js/*.js',
    );

    $app['assetic.output.path_to_js'] = 'js/fnsilex.js';