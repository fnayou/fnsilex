<?php

/**
 * Configuration for test environment
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */
    // will enable debug mod
    $app['debug'] = true;
