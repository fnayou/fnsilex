<?php

/**
 * Configuration for development environment
 *
 * @package fnSilex
 * @author Aymen Fnayou <developer@aymen-fnayou.com>*
 * @version 0.1
 */

    // will load production configuration
    require __DIR__.'/prod.php';

    // and enable debug mod
    $app['debug'] = true;
